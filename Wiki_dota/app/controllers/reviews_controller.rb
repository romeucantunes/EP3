class ReviewsController < ApplicationController
  before_action :set_review, only: [:edit, :update, :destroy]
  before_action :set_hero
  before_action :authenticate_user!
  before_action :check_user, only: [:edit, :update, :destroy]

  respond_to :html


  def new
    @review = Review.new
    respond_with(@review)
  end

  def edit
  end

  def create
    @review = Review.new(review_params)
    @review.user_id = current_user.id
    @review.hero_id = @hero.id

    @review.save
    respond_with(@hero)
  end

  def update
    @review.update(review_params)
    respond_with(@review)
  end

  def destroy
    @review.destroy
    respond_to do |format|
      format.html { redirect_to hero_path(@hero), notice: 'Comentário apagado com sucesso'}
      format.json { head :no_content}
    end
  end

  private
    def set_review
      @review = Review.find(params[:id])
    end

    def set_hero
      @hero = Hero.find(params[:hero_id])
    end

    def check_user
      unless (@review.user == current_user) || (current_user.admin?)
        redirect_to root_url, alert: "Esse comentário pertence a outra pessoa."
      end
    end

    def review_params
      params.require(:review).permit(:rating, :comment)
    end
end
